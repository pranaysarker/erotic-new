<?php 
	/* Template Name: Test template */
	get_header();
 ?>
 <div class="main-page">
	<div class="container">
		<div class="shop-container">
			<div class="product-container">

				<!-- product starts here -->
				<div class="product">
					<div class="row">
						<div class="col-md-7">
							<div class="d-flex justify-content-start align-items-center">
								<div class="d-flex justify-content-start greentick-img">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/greentick.png" alt="">
									<b>A+</b>
									<b>RATING</b>
								</div>
								<div class="">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/buy-7-header.png" alt="">
								</div>
							</div>
							<div class="product-image">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/product.png" alt="">
							</div>
						</div>
						<div class="col-md-5">
							<h2 class="text-uppercase mb-2">7 Month Supply <span class="d-block text-bold font-weight-bold text-custom">SAVE OVER $330!</span></h2>
							<div class="row">
								<div class="col p-0">
									<img class="float-left goldentick" src="<?php echo get_template_directory_uri(); ?>/assets/images/golden-tick.png" alt=""><span>Normally $70/Bottle</span>
								</div>
								<div class="col p-0">
									<img class="float-left goldentick" src="<?php echo get_template_directory_uri(); ?>/assets/images/golden-tick.png" alt=""><span>EXTREME GROWTH!</span>
								</div>
							</div>
							<div class="row">
								<div class="col p-0">
									<img class="float-left goldentick" src="<?php echo get_template_directory_uri(); ?>/assets/images/golden-tick.png" alt=""><span>A $490 Value</span>
								</div>
								<div class="col p-0">
									<img class="float-left goldentick" src="<?php echo get_template_directory_uri(); ?>/assets/images/golden-tick.png" alt=""><span>3 BOTTLES FREE!</span>
								</div>
							</div>
					        
					        <div class="d-flex justify-content-start minus-15">
					        	<div><img class="float-left goldentick" src="<?php echo get_template_directory_uri(); ?>/assets/images/golden-tick.png" alt=""> <span>100% MONEY BACK GUARANTEE!</span></div>
					        </div>
					        <div class="was d-flex justify-content-start ">
					        	<span class="font-weight-bold"><del>WAS $490.00</del> </span><span class="pl-4 text-uppercase font-weight-bold text-danger"> Save $330.05</span>
					        </div>
					        <div class="order-area row d-flex align-items-center my-4">
								<div class="price col-auto">
									<span class="text-uppercase text-custom small">
										now only
										<br>
									</span>
									<span class="font-weight-bold bold text-uppercase h3">
										159$
									</span>
								</div>
								<div class="col">
									<a href="#"><img class="mt-2" src="<?php echo get_template_directory_uri(); ?>/assets/images/redorder.png" alt=""></a>
								</div>
					        </div>
						</div>
					</div>
				</div>
				<!-- product ends here -->

			</div>
		</div>
	</div>
 </div>
 <?php get_footer(); ?>