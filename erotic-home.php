<?php /* Template Name: Home-page */ ?>
<?php get_header(); ?>


	<section>  <!-- slider start  here -->
		 <div class="container">
		 	<div class="row">
		 		<div class="col-md-12 p-0 m-0 try-it">
		 			<img class="img-fluid" src="<?php if( !empty(cs_get_option('homHeader_big_image')) ){
		 				echo cs_get_option('homHeader_big_image');
		 			} ?>" alt="">
		 			<a href="<?php if( !empty(cs_get_option('homHeader_big_image_link')) ){
		 				echo cs_get_option('homHeader_big_image_link');
		 			} ?>" class="hov"></a>
		 		</div>
		 	</div>
		 </div>
	</section>  <!-- slider end here -->

<section>
    <div class="container">
        <div class="row">
         
			<div class="col-md-4 p-0 m-0">
				<div class="promo">
				<div class="card" >
					<img class="card-img-top" src="<?php if( !empty(cs_get_option('promo_one_image')) ){
		 				echo cs_get_option('promo_one_image');
		 			} ?>" alt="Card image cap">
				    <div class="card-body">
			    	<?php if( !empty(cs_get_option('promo_one_content')) ){
		 				echo cs_get_option('promo_one_content');
		 			} ?>
				    </div>
				</div>
			</div>
			</div>
			<div class="col-md-4 p-0 m-0">
				<div class="promo">
				<div class="card" >
					<img class="card-img-top" src="<?php if( !empty(cs_get_option('promo_two_image')) ){
		 				echo cs_get_option('promo_two_image');
		 			} ?>" alt="Card image cap">
				    <div class="card-body">
			    	<?php if( !empty(cs_get_option('promo_two_content')) ){
		 				echo cs_get_option('promo_two_content');
		 			} ?>
				    </div>
			    </div>
			</div>

			</div>
			<div class="col-md-4 p-0 m-0">
				<div class="promo">
				<div class="card" >
					<img class="card-img-top" src="<?php if( !empty(cs_get_option('promo_three_image')) ){
		 				echo cs_get_option('promo_three_image');
		 			} ?>" alt="Card image cap">
				    <div class="card-body">
			    	<?php if( !empty(cs_get_option('promo_three_content')) ){
		 				echo cs_get_option('promo_three_content');
		 			} ?>
				    </div>
				</div>
			</div>
          </div>
		</div>
		</div>
	</div>
</section>


<section>
		 <div class="container">
		 	<div class="row justify-content-center">
		 	  <div class="col-md-12">
		 		<div class="ero_text">
		 		<h1 class="extra-big-text"><?php if( !empty(cs_get_option('bigTitle_first_title')) ){
		 				echo cs_get_option('bigTitle_first_title');
		 			} ?></h1>
		 		<p class="body-text font-weight-normal line-height-2"><?php if( !empty(cs_get_option('bigTitle_first_subTitle')) ){
		 				echo cs_get_option('bigTitle_first_subTitle');
		 			} ?></p>

		 			</div>
		 		</div>
		 	</div>
		 </div>
	</section>


<section>
	<div class="container">
		<div class="row justify-content-center">



			<div class="col-md-4 p-5 m-0">
				
			<img src="<?php if( !empty(cs_get_option('bigTitle_first_image')) ){
		 				echo cs_get_option('bigTitle_first_image');
		 			} ?>" alt="">
		 	</div>


		   <div class="col-md-8 p-0 m-0 py-5">
				<?php if( !empty(cs_get_option('bigTitle_first_list')) ):
		 				foreach( cs_get_option('bigTitle_first_list') as $values ): ?>
				<div class="card">
				  <div class="card-body">
				    <p class="card-text"><strong><?php if( !empty($values['bigTitle_first_list_strong']) ){
				    	echo $values['bigTitle_first_list_strong'];
				    } ?></strong> <?php if( !empty($values['bigTitle_first_list_normal']) ){
				    	echo $values['bigTitle_first_list_normal'];
				    } ?></p>
				  </div>
				</div>
		 		<?php endforeach; endif; ?>
				<!-- <div class="card">
				  <div class="card-body">
				    <p class="card-text"><strong>Tribulus Terrestris:</strong> Tribulus Terrestris is a famous herb. One huge benefit is that it stimulates testosterone in both men and women. Why is this important? Because higher testosterone levels lead to a higher sex drive.</p>
				  </div>
				</div>

				<div class="card">
				 
				  <div class="card-body">
				    <p class="card-text"><strong>Avena Sativa:</strong> Often described as “Nature’s Viagra”, Avena Sativa is soothing and calming to the nervous system, whilst at the same time increasing sexual desire in men and women.</p>
				  </div>
				</div>

				<div class="card">
				  
				  <div class="card-body">
				    <p class="card-text"><strong>Cuscuta chinensis:</strong> Cuscuta seed has a high content of flavonoids and has strong antioxidant properties. It has been found in studies to have positive effects on sperm health and motility, invigorates the reproductive system, and has antioxidant benefits. Crataegus.</p>
				  </div>
				</div>

				<div class="card">
				  
				  <div class="card-body">
				    <p class="card-text"><strong>oxycantha :</strong> Crataegus oxycanthais one of the popular male enhancement extracts. By improving blood flow, Crataegus oxycantha has a direct impact on erectile response. It enables more blood to pass through small vessels like the ones in the penis.</p>
				  </div>
				</div>

				<div class="card">
				 
				  <div class="card-body">
				    <p class="card-text"><strong>Panax ginseng:</strong> Ginseng has been utilized for millennia to enhance physical performance and longevity. It is thought to support male sexual health and erections.</p>
				  </div>
				</div>

				<div class="card">
				 
				  <div class="card-body">
				    <p class="card-text"><strong>repens :</strong> Saw palmetto is a popular herb among men because preliminary research has found it beneficial for prostate health. Supplementation with saw palmetto increase certain forms of testosterone.</p>
				  </div>
				</div>

				<div class="card">
				  
				  <div class="card-body">
				    <p class="card-text"><strong>Ginkgo biloba:</strong> This herbal ingredient support optimal circulation, an important factor in male sexual performance.</p>
				  </div>
				</div>

				<div class="card">
				  
				  <div class="card-body">
				    <p class="card-text"><strong>Capsicum annuum:</strong> Capsicum annuum were used in ancient times to increase female libido man’s sexual desire Horny goat : Horny goat weed is a herb that has been a traditional remedy in China for centuries. It’s used for low libido, erectile dysfunction, fatigue, pain, and other conditions.</p>
				  </div>
				</div> -->
		 	</div>

		</div>
	</div>
</section>



	<section>
 		 <div class="container">
 		 	<div class="row">
 		 		<div class="col-md-12 p-0 m-0">
 		 			<img class="img-fluid" src="<?php  echo get_template_directory_uri();  ?>/assets/images/testimg.jpg" alt="">
 		 		</div>
 		 		
 		 	</div>
 		 </div>
 	</section>

	
	<div class="section-devider"></div>
	<section>
		 <div class="container">
		 	<div class="row">
		 		<div class="col-md-3 py-5">
		 			<img class="m-5" src="<?php if( !empty(cs_get_option('bigTitle_second_image')) ){
		 				echo cs_get_option('bigTitle_second_image');
		 			} ?>" alt="">
		 		</div>
		 		<div class="col-md-9">
		 			<h1 class="biggest-text text-uppercase theme-font"><?php if( !empty(cs_get_option('bigTitle_second_title')) ){
		 				echo cs_get_option('bigTitle_second_title');
		 			} ?>
		 			</h1>
		 			<h1 class="pl-5 ml-2 mt-3 font-italic font-weight-bold"><?php if( !empty(cs_get_option('bigTitle_second_subTitle')) ){
		 				echo cs_get_option('bigTitle_second_subTitle');
		 			} ?></h1>
		 			
		 			
		 			<div class="row pl-5">
		 				<div class="col-md-12 m-0 p-0">
		 					<ul class="service-list">         
		 					<?php if( !empty( cs_get_option('bigTitle_second_list') ) ): 
		 						foreach( cs_get_option('bigTitle_second_list') as $value ) :
		 							if( !empty($value['bigTitle_second_list_normal']) ):
		 					?>
			                    <li><img src="<?php echo get_template_directory_uri();  ?>/assets/images/goldtick.png" alt="Gold Tick"><span><?php 
			                    	echo $value['bigTitle_second_list_normal'];
			                     ?></span></li>
							<?php endif; endforeach; endif; ?>

			                    <!-- 
			                    <li><img src="<?php //echo get_template_directory_uri();  ?>/assets/images/goldtick.png" alt="Gold Tick"><span>Would you like to be known as a guy with a big penis?</span></li>
			                    <li><img src="<?php //echo get_template_directory_uri();  ?>/assets/images/goldtick.png" alt="Gold Tick"><span>Do you wish your penis were wider?</span></li>
			                    <li><img src="<?php //echo get_template_directory_uri();  ?>/assets/images/goldtick.png" alt="Gold Tick"><span>Would you like to dominate any bedroom you walk in to?</span></li>
			                    <li><img src="<?php //echo get_template_directory_uri();  ?>/assets/images/goldtick.png" alt="Gold Tick"><span>Are you embarrassed by the size of your penis?</span></li>
			                    <li><img src="<?php //echo get_template_directory_uri();  ?>/assets/images/goldtick.png" alt="Gold Tick"><span>Do you wonder if the guy she was with before you was bigger?</span></li>
			                    <li><img src="<?php //echo get_template_directory_uri();  ?>/assets/images/goldtick.png" alt="Gold Tick"><span>Do you image how great your sex life would be with a larger penis?</span></li>  -->                  
			                </ul>
		 				</div>
		 			</div>
		 			<div class="row">
		 				<div class="col-md-12">
		 					<div class="red-big-btn">
		 						<a href="<?php if( !empty( cs_get_option('bigTitle_second_buy_link') ) ){
		 								echo cs_get_option('bigTitle_second_buy_link');
		 							} ?>">
		 							<img src="<?php if( !empty( cs_get_option('bigTitle_second_buy') ) ){
		 								echo cs_get_option('bigTitle_second_buy');
		 							} ?>" alt="">
		 						</a>
		 				
		 			</div>
		 		</div>
		 	</div>
		 </div>
	</section>
	<div class="section-devider"></div>
	
    <section>
		 <div class="container">
		 	<div class="row justify-content-center">
		 		<div class="col-md-10">
		 		    <div class="order_text">
		 			<h1 class="extra-big-text">
					<?php if( !empty(cs_get_option('homepage_product_section_title'))){
						echo cs_get_option('homepage_product_section_title');
					} ?>
		 			</h1>
		 			</div>
		 			<div class="price-area">
		 				<?php if( !empty(cs_get_option('homepage_produc_section_product')) ): 
		 					foreach (cs_get_option('homepage_produc_section_product') as $value) :
		 				?>
						<div class="single-price">
		 					<img src="<?php if( !empty( $value['product_image'] ) ){
		 						echo $value['product_image'];
		 					} ?>" alt="">
		 					<a href="<?php if( !empty( $value['product_buy_link'] ) ){
		 						echo $value['product_buy_link'];
		 					} ?>"><img src="<?php if( !empty( $value['product_buy_image'] ) ){
		 						echo $value['product_buy_image'];
		 					} ?>" alt="" class="buy-btn"></a>
		 					<div class="pricing-content">
		 						<h1><?php if( !empty( $value['product_sel_price'] ) ){
		 						echo '$'.$value['product_sel_price'];
		 					} ?> <span class="offer"><?php if( !empty( $value['product_regular_price'] ) && !empty( $value['product_sel_price'] ) ){
		 						$priceNow = $value['product_regular_price']-$value['product_sel_price'];
		 						echo 'YOU SAVE $'.$priceNow;
		 					} ?></span> <span class="list-price"><?php if( !empty( $value['product_regular_price'] ) ){
		 						echo 'LIST PRICE $'.$value['product_regular_price'];
		 					} ?></span></h1>
		 					</div>
		 				</div>
		 				<?php endforeach; endif; ?>
		 				<!-- <div class="single-price">
		 					<img src="http://www.erotictouch.shop/wp-content/uploads/2018/05/Untitled-1.png" alt="">
		 					<img src="<?php echo get_template_directory_uri();  ?>/assets/images/buy-now.png" alt="" class="buy-btn">
		 					<div class="pricing-content">
		 						<h1>$39.95 <span class="offer">YOU SAVE $30</span> <span class="list-price">LIST PRICE $69.95</span></h1>
		 					</div>
		 				</div>
		 				<div class="single-price">
		 					<img src="http://www.erotictouch.shop/wp-content/uploads/2018/05/2.png" alt="">
		 					<img src="<?php echo get_template_directory_uri();  ?>/assets/images/buy-now.png" alt="" class="buy-btn">
		 					<div class="pricing-content">
		 						<h1>$119.95<span class="offer">YOU SAVE $230</span> <span class="list-price">LIST PRICE $359.95</span></h1>
		 					</div>
		 				</div>
		 				<div class="single-price">
		 					<img src="http://www.erotictouch.shop/wp-content/uploads/2018/05/3.png" alt="">
		 					<img src="<?php echo get_template_directory_uri();  ?>/assets/images/buy-now.png" alt="" class="buy-btn">
		 					<div class="pricing-content">
		 						<h1>$79.95<span class="offer">YOU SAVE $130</span> <span class="list-price">LIST PRICE $209.95</span></h1>
		 					</div>
		 				</div> -->
		 			</div>
		 		</div>
		 	</div>
		 </div>
	</section>
	
<?php get_footer(  ); ?>