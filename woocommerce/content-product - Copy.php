<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product,$product_object;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

	$regular_price 	= $product->regular_price;
	$sale_price 	= $product->sale_price;
?>
<!-- product starts here -->
				<div <?php wc_product_class('product col-md-3'); ?>>
					<div class="row">
						<div class="">
							<div class="d-flex justify-content-start align-items-center">
								<div class="d-flex justify-content-start greentick-img">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/greentick.png" alt="">
									<b>A+</b>
									<b>RATING</b>
								</div>
								<div class="">
									<?php $galleryId = $product->get_gallery_attachment_ids();

										echo wp_get_attachment_image( $galleryId[0],'full' );
									 ?>
								</div>
							</div>
							<div class="product-image">
								<?php woocommerce_template_loop_product_thumbnail(); ?>
							</div>
						</div>
						<div class="">
							<h2 class="text-uppercase mb-2"><?php 
								the_title(); 
							?> <span class="d-block text-bold font-weight-bold text-custom">SAVE OVER $<?php echo $regular_price - $sale_price; ?>!</span></h2>
							<div class="row">
								<div class="col p-0">
									<img class="float-left goldentick" src="<?php echo get_template_directory_uri(); ?>/assets/images/golden-tick.png" alt=""><span>Normally $70/Bottle</span>
								</div>
								<div class="col p-0">
									<img class="float-left goldentick" src="<?php echo get_template_directory_uri(); ?>/assets/images/golden-tick.png" alt=""><span>EXTREME GROWTH!</span>
								</div>
							</div>
							<div class="row">
								<div class="col p-0">
									<img class="float-left goldentick" src="<?php echo get_template_directory_uri(); ?>/assets/images/golden-tick.png" alt=""><span>A $<?php echo $regular_price; ?> Value</span>
								</div>
								<div class="col p-0">
									<img class="float-left goldentick" src="<?php echo get_template_directory_uri(); ?>/assets/images/golden-tick.png" alt=""><span>3 BOTTLES FREE!</span>
								</div>
							</div>
					        
					        <div class="d-flex justify-content-start minus-15">
					        	<div><img class="float-left goldentick" src="<?php echo get_template_directory_uri(); ?>/assets/images/golden-tick.png" alt=""> <span>100% MONEY BACK GUARANTEE!</span></div>
					        </div>
					        <div class="was d-flex justify-content-start ">
					        	<span class="font-weight-bold"><del>WAS $<?php echo $regular_price; ?>.00</del> </span><span class="pl-4 text-uppercase font-weight-bold text-danger"> Save $<?php echo $regular_price - $sale_price; ?></span>
					        </div>
					        <div class="order-area row d-flex align-items-center my-4">
								<div class="price col-auto">
									<span class="text-uppercase text-custom small">
										now only
										<br>
									</span>
									<span class="font-weight-bold bold text-uppercase h3">
										$<?php echo $sale_price; ?>
									</span>
								</div>
								<div class="col">
									<a href="<?php echo esc_url( $product->add_to_cart_url() ); ?>"><img class="mt-2" src="<?php echo get_template_directory_uri(); ?>/assets/images/redorder.png" alt=""></a>
									<?php woocommerce_template_loop_add_to_cart(); ?>
								</div>
					        </div>
						</div>
					</div>
				</div>
				<!-- product ends here -->

