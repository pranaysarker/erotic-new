<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product,$product_object;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

	$regular_price 	= isset( $product->regular_price )? $product->regular_price : 0;
	$sale_price 	= isset($product->sale_price)? $product->sale_price : 0;
?>
<!-- product starts here -->
				<div <?php wc_product_class('product col-md-3 p-0 px-0 my-3'); ?>>
					<div class="pr-cont">
						<div class="title text-center py-3 bg-gray">
							<h1><?php the_title(); ?></h1>
						</div>
						<div class="product-image py-3">
							<?php woocommerce_template_loop_product_thumbnail(); ?>
						</div>
						<div class="tick px-1">
							<ul class="tick-ul">
					            <li><i class="fa fa-check" style="color:#ff630e;"></i>&nbsp;&nbsp;A $<?php echo $regular_price; ?> Value</li>
					            <li><i class="fa fa-check" style="color:#ff630e;"></i>&nbsp;&nbsp;100% Money-Back Guarantee</li>
					            <li><i class="fa fa-check" style="color:#ff630e;"></i>&nbsp;&nbsp;SAVE OVER $<?php echo $regular_price - $sale_price; ?>!</li>
					        </ul>
						</div>
						<div class="price p-3">
							<b class="d-block only">Only</b>
							<div class="d-block text-center price-dolar">$<?php echo $sale_price ?></div>
							<del class="d-block text-center">ORIGINAL PRICE: $<?php echo $regular_price; ?></del>
						</div>
						<div class="cart-button pt-1 pb-4">
							<a class="d-block text-center" href="<?php echo esc_url( $product->add_to_cart_url() ); ?>"><img class="mt-2 w-auto my-auto" src="<?php echo get_template_directory_uri(); ?>/assets/images/add-to-cart-red.png" alt=""></a>
						</div>
					</div>
				</div>
				<!-- product ends here -->

