<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */
defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
	do_action( 'woocommerce_before_main_content' );

?>

 <div class="main-page">
	<div class="container">
		<div class="shop-container">
			<div class="product-container row">
<?php
if ( woocommerce_product_loop() ) {

	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked wc_print_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	// do_action( 'woocommerce_before_shop_loop' );

	woocommerce_product_loop_start();

	if ( wc_get_loop_prop( 'total' ) ) {
		while ( have_posts() ) {
			the_post();

			/**
			 * Hook: woocommerce_shop_loop.
			 *
			 * @hooked WC_Structured_Data::generate_product_data() - 10
			 */
			do_action( 'woocommerce_shop_loop' );

			wc_get_template_part( 'content', 'product' );
		}
	}

	woocommerce_product_loop_end();

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
?>
			</div>
		</div>
	</div>
	
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/gold-bar-small.jpg" alt="">
		</div>
		<div class="col-md-8">
			<p class="h3 font-weight-normal">Warwick Biological Labs</p>
			<h1>The <span class="text-warning font-weight-bold">Gold Standard</span> of Guarantees</h1>
			<p>We are so confident that you are going to be thrilled with the results that we are happy to provide an <span class="font-weight-bold">Unrivaled, Best-In-Class, Gold Standard 100% Money Back Guarantee</span>. <u>No games, no fine print, no dodging you when you call or email</u>, simply put - if for any reason you are not fully satisfied with our product, you can receive a full refund of your purchase price (less shipping and processing fees) within <span class="font-weight-bold">90 days</span> of purchase. That’s 3 months from your purchase date to evaluate if Predoxen is working for you.</p>

<p class="py-3">Unlike many other fly-by-night male enhancement companies, Predoxen is the only company backed by a real nutraceutical manufacturer, Warwick Biological Labs, and so we can offer an unmatched 90 day guarantee.</p>
		</div>
		<div class="col-md-12">
			<hr>
			<h3>We want to see you happy and if for ANY reason you are not experiencing the following results insist you return the product for a FULL REFUND:</h3>
		</div>
		<div class="col-md-4">
			<img class="py-5" src="<?php echo get_template_directory_uri(); ?>/assets/images/3months-1.png" alt="">
		</div>
		<div class="col-md-8">
			<div class="tick big px-1 py-5">
				<ul class="tick-ul">
		            <li><i class="fa fa-check" style="color:#ff630e;"></i>&nbsp;&nbsp;Wider, Thicker and Bigger Erections</li>
		            <li><i class="fa fa-check" style="color:#ff630e;"></i>&nbsp;&nbsp;Harder Erections</li>
		            <li><i class="fa fa-check" style="color:#ff630e;"></i>&nbsp;&nbsp;More Frequent Erections</li>
		            <li><i class="fa fa-check" style="color:#ff630e;"></i>&nbsp;&nbsp;Increased Stimulation & Sensations</li>
		            <li><i class="fa fa-check" style="color:#ff630e;"></i>&nbsp;&nbsp; More Energy</li>
		            <li><i class="fa fa-check" style="color:#ff630e;"></i>&nbsp;&nbsp;Quicker Recovery for Multiple Rounds</li>
		            <li><i class="fa fa-check" style="color:#ff630e;"></i>&nbsp;&nbsp;Increased Confidence</li>
		        </ul>
			</div>
		</div>
		<div class="col-md-12">
			<hr>
			<p class="h4 mt-4">If for any reason you are not 100% thrilled with the results of Predoxen simply call us at (800) 800-341-8509 Monday - Friday, 8AM - 8PM EST and Saturday 8AM - 1PM EST or email us at <b>support@predoxen.com</b> and one of our representative will be happy to assist you</p>.
			<p class="h4 mt-4">We want you to love our product and are confident you will be calling to order more in no time.</p>
			<hr>
			<p class="h3">NEVER Any Monthly Billing Schemes!</p>
			<p class="h4 mt-4 mb-5">You can be confident in placing your Predoxen order with us. Warwrick Biological Labs stands behind our commitment to creating a safe environment for our customers to place orders. What you order today is the only thing you will be billed and shipped. We will never place you into any automatic monthly billing programs.</p>
		</div>
	</div>
</div>


</div>
<?php
do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
// do_action( 'woocommerce_sidebar' );

get_footer( 'shop' );
