<?php
	/* Template Name: Contact-page */
	get_header();
?>
<div class="main-page">
	<div class="shop-container">
		<div class="contact-page-container">
			
			<div class="contac-top">
				<div class="container">
					<div class="contact-logo d-flex justify-content-between align-items-center">
						<div class="logo">
							<?php the_custom_logo(  ); ?>
						</div>
						<div class="security">
							<img src="<?php if( !empty( cs_get_option('contact_page_security_image') ) ){
								echo cs_get_option('contact_page_security_image');
							} ?>" alt="">
						</div>
						<div class="guarantee">
							<img src="<?php if( !empty( cs_get_option('contact_page_guarantee_image') ) ){
								echo cs_get_option('contact_page_guarantee_image');
							} ?>" alt="">
						</div>
					</div>
					<div class="contant-phonetext text-center py-3">
						<div class="call-text h1 font-weight-bold">
							<span class="text"><?php if( !empty( cs_get_option('contact_page_phoneUs_text') ) ){
								echo cs_get_option('contact_page_phoneUs_text');
							} ?></span>
							<span class="number"><a href="tel:<?php if( !empty( cs_get_option('contact_page_phoneUs_number') ) ){
								$number = str_replace( '-','',cs_get_option('contact_page_phoneUs_number'));
								echo $number;
							} ?>" rel="nofollow"><?php if( !empty( cs_get_option('contact_page_phoneUs_number') ) ){
								echo cs_get_option('contact_page_phoneUs_number');
							} ?></a></span>
						</div>
					</div>
				</div>
			</div>
			<div class="content-main">
				<div class="container">
					<div class="contact-container py-5">
						<div class="contact-body page-container">
							<div class="row">
								<div class="col-md-8 has-icon">
									<?php if( !empty( cs_get_option('contact_page_top_text') ) ){
										echo cs_get_option('contact_page_top_text');
									} ?>
									<div class="display-1 font-weight-bold text-uppercase font-BebasNeue text-center">
									<?php if( !empty( cs_get_option('contact_page_form_title') ) ){
										echo cs_get_option('contact_page_form_title');
									} ?>
									</div>
									<div class="contact-form table-responsive contac-table px-1 py-5">
											<!-- <tr>
												<td>Name</td>
												<td><input type="text"></td>
											</tr>
											<tr>
												<td>Email</td>
												<td><input type="text"></td>
											</tr>
											<tr>
												<td>Message</td>
												<td><textarea name=""></textarea></td>
											</tr> -->
											<?php 
												echo do_shortcode( '[contact-form-7 id="10873" title="contact-form"]', $ignore_html = true ); 
											?>
									</div>
								</div>
								<div class="col-md-4">
									<img src="<?php if( !empty( cs_get_option('contact_page_sidebar_image') ) ){
										echo cs_get_option('contact_page_sidebar_image');
									} ?>" alt="">
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>