<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title><?php bloginfo( 'name' ); ?></title>

	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/text-fonts.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/magnific-popup.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/responsive.css">
	<?php wp_head(); ?>
</head>
<body>
	<header class="bg-dark">
		<div class="container">
			<div class="row">
				<div class="col-md-12 p-0 m-0">
					<nav class="navbar navbar-expand-md navbar-light p-0 m-0">
						<button class="navbar-toggler theme-bg" type="button" data-toggle="collapse" data-target="#headermenu">
							<span class="navbar-toggler-icon "></span>
						</button>
						<div class="collapse navbar-collapse p-0 m-0 text-center" id="headermenu">

							<?php wp_nav_menu( array(
								'menu'            => 'main-menu',
								'theme_location'  => 'main-menu',
								'container'       => '',
								'container_class' => '',
								'container_id'    => '',
								'menu_class'      => 'navbar-nav text-center m-auto',
								'fallback_cb'     => 'wp_page_menu',
								'walker'          => new Erotic_nav_walker,
							) ); ?>
							<!-- <ul class="navbar-nav text-center m-auto">
								<li class="nav-item">
									<a href="index.html" class="text-white nav-link text-uppercase h3 m-0 p-0 active">Home</a>
								</li>
								<li class="nav-item">
									<a href="#" class="text-white nav-link text-uppercase h3 m-0 p-0">How It Works?</a>
								</li>
								<li class="nav-item">
									<a href="#" class="text-white nav-link text-uppercase h3 m-0 p-0">Faq</a>
								</li>
								<li class="nav-item">
									<a href="#" class="text-white nav-link text-uppercase h3 m-0 p-0">Testimonials</a>
								</li>
								<li class="nav-item">
									<a href="#" class="text-white nav-link text-uppercase h3 m-0 p-0">Contact Us</a>
								</li>
								<li class="nav-item">
									<a href="#" class="text-white nav-link text-uppercase h3 m-0 p-0">Order Now</a>
								</li>
							</ul> -->
						</div>
					</nav>
				</div>
			</div>
		</div>
	</header>