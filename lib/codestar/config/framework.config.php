<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK SETTINGS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$settings           = array(
  'menu_title'      => 'Theme Options',
  'menu_type'       => 'menu', // menu, submenu, options, theme, etc.
  'menu_slug'       => 'cs-framework',
  'ajax_save'       => false,
  'show_reset_all'  => false,
  'framework_title' => 'Erotic Options <small>by Codestar</small>',
);

// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options        = array();

// ----------------------------------------
// a option section for options overview  -
// ----------------------------------------
/***
* Home page options
*/
$options[]      = array(
  'name'  => 'homepage_options',
  'title' => 'HomePage Options',
  'icon'  => 'fa fa-star',
  'fields'  => array(
    array(
      'type'    => 'notice',
      'class'   => 'success',
      'content' => 'Homepage top',
    ),
    array(
      'id'        => 'homHeader_big_image',
      'title'     => 'Home top Big image',
      'type'      => 'upload',
      'default'   => 'http://www.erotictouch.shop/wp-content/uploads/2018/05/IMGES-1.jpg',
      'desc' => 'Home page top bit image.',
    ),
    array(
      'id'        => 'homHeader_big_image_link',
      'title'     => 'Home top Big image link',
      'type'      => 'text',
      'default'   => 'erotictouch.shop/shop',
      'desc' => 'Home page top bit image link.',
    ),
    array(
      'type'    => 'notice',
      'class'   => 'success',
      'content' => 'Promo section',
    ),
    //promo one
    array(
      'id'        => 'promo_one_image',
      'title'     => 'Promo One Image',
      'type'      => 'upload',
      'default'   => 'http://www.erotictouch.shop/wp-content/uploads/2018/05/woman_with-headset.png',
      'desc'      => 'Select promo one image.',
    ),
    array(
      'id'        => 'promo_one_content',
      'title'     => 'Promo One Content',
      'type'      => 'wysiwyg',
      'default'   => '<h2>Order by Phone</h2><p class="">(718) 618-9649 Mon Sat 10:00 am to 6:00 pm</p>',
      'desc'      => 'Promo one Content.',
    ),
    //promo two
    array(
      'id'        => 'promo_two_image',
      'title'     => 'Promo two Image',
      'type'      => 'upload',
      'default'   => 'http://www.erotictouch.shop/wp-content/uploads/2018/05/freeshipping-4.jpg',
      'desc'      => 'Select promo two image.',
    ),
    array(
      'id'        => 'promo_two_content',
      'title'     => 'Promo two Content',
      'type'      => 'wysiwyg',
      'default'   => '<h2>Same Day Dispatch</h2><p class="">We ship same day orderplaced by 11 am.</p>',
      'desc'      => 'Promo two Content.',
    ),
    //promo three
    array(
      'id'        => 'promo_three_image',
      'title'     => 'Promo three Image',
      'type'      => 'upload',
      'default'   => 'http://www.erotictouch.shop/wp-content/uploads/2018/05/same-day-truck-4.png',
      'desc'      => 'Select promo three image.',
    ),
    array(
      'id'        => 'promo_three_content',
      'title'     => 'Promo three Content',
      'type'      => 'wysiwyg',
      'default'   => '<h2>Get Free Shopping</h2><p class="card-text"> On All Order Over $100</p>',
      'desc'      => 'Promo three Content.',
    ),
    // big title first section
    array(
      'type'    => 'notice',
      'class'   => 'success',
      'content' => 'Big title first section.',
    ),
    array(
      'id'        => 'bigTitle_first_title',
      'title'     => 'Big title first section title',
      'type'      => 'text',
      'default'   => 'Erotic Touch® provides a proprietary blend of 10 unique, natural ingredients',
      'desc'      => 'Big title first section title. Big title.',
    ),
    array(
      'id'        => 'bigTitle_first_subTitle',
      'title'     => 'Big title first section sub title',
      'type'      => 'wysiwyg',
      'default'   => 'A unique blend of eight herbal ingredients traditionally used to support men’s sexual health and virility. Features a powerful dose of standardized epimedium, also known as horny goat weed, as well as potent doses of avena sativa, tribulus, ginkgo biloba, damiana, ginkgo and ginseng.',
      'desc'      => 'Big title first section sub title.',
    ),
    array(
      'id'        => 'bigTitle_first_image',
      'title'     => 'Big title first section image',
      'type'      => 'upload',
      'default'   => 'http://www.erotictouch.shop/wp-content/uploads/2018/05/Erotic-touch-3.png',
      'desc'      => 'Big title first section image. Big image.',
    ),
    array(
      'id'              => 'bigTitle_first_list',
      'type'            => 'group',
      'title'           => 'Big title first section list',
      'button_title'    => 'Add New',
      'accordion_title' => 'Add New list',
      'fields'          => array(
        array(
          'id'    => 'bigTitle_first_list_strong',
          'type'  => 'text',
          'title' => 'Strong Text',
        ),
        array(
          'id'    => 'bigTitle_first_list_normal',
          'type'  => 'text',
          'title' => 'Normal Text',
        ),
      ),
    ),
    // big title second section
    array(
      'type'    => 'notice',
      'class'   => 'success',
      'content' => 'Big title second section.',
    ),
    array(
      'id'        => 'bigTitle_second_title',
      'title'     => 'Big title second section title',
      'type'      => 'wysiwyg',
      'default'   => 'being a <strong>bigger man</strong> is<br>easy with Erotic Touch<span style="font-size: 4vmin; line-height: 7vmin; vertical-align: text-top;">®</span>',
      'desc'      => 'Big title second section title. Big title.',
    ),
    array(
      'id'        => 'bigTitle_second_subTitle',
      'title'     => 'Big title second section sub title',
      'type'      => 'wysiwyg',
      'default'   => '"Erotic Touch@ provides a natural herbal approach to supporting male virility,that<br><u>  help support optimal hormonal health  </u>and circulation safely and naturally."',
      'desc'      => 'Big title second section sub title.',
    ),
    array(
      'id'        => 'bigTitle_second_image',
      'title'     => 'Big title second section image',
      'type'      => 'upload',
      'default'   => 'http://localhost/sajidvai-new/wp-content/themes/erotic-theme/assets/images/home_bigger_couple.jpg',
      'desc'      => 'Big title second section image. Big image.',
    ),
    array(
      'id'              => 'bigTitle_second_list',
      'type'            => 'group',
      'title'           => 'Big title first section list',
      'button_title'    => 'Add New',
      'accordion_title' => 'Add New list',
      'fields'          => array(
        array(
          'id'    => 'bigTitle_second_list_normal',
          'type'  => 'text',
          'title' => 'List text',
        ),
      ),
    ),
    array(
      'id'        => 'bigTitle_second_buy',
      'title'     => 'Big title second buy button',
      'type'      => 'upload',
      'default'   => 'http://localhost/sajidvai-new/wp-content/themes/erotic-theme/assets/images/home_spg_btn.jpg',
      'desc'      => 'Try it button image.',
    ),
    array(
      'id'        => 'bigTitle_second_buy_link',
      'title'     => 'Big title second buy button link',
      'type'      => 'text',
      'default'   => 'http://www.erotictouch.shop/shop/',
      'desc'      => 'Try it button image link.',
    ),
    // home-page products section

    array(
      'type'    => 'notice',
      'class'   => 'success',
      'content' => 'Homepage Products section',
    ),
    array(
      'id'        => 'homepage_product_section_title',
      'title'     => 'Homepage product section title',
      'type'      => 'textarea',
      'default'   => 'order Erotic Touch today',
      'desc'      => 'Homepage product section title.',
    ),
    array(
      'id'              => 'homepage_produc_section_product',
      'type'            => 'group',
      'title'           => 'Add product',
      'button_title'    => 'Add new',
      'accordion_title' => 'Add New product',
      'fields'          => array(
        array(
          'id'    => 'product_image',
          'type'  => 'upload',
          'title' => 'Product image',
          'default' => 'http://www.erotictouch.shop/wp-content/uploads/2018/05/Untitled-1.png'
        ),
        array(
          'id'    => 'product_buy_image',
          'type'  => 'upload',
          'title' => 'Product buy image',
          'default' => 'http://www.erotictouch.shop/wp-content/themes/erotic-theme/assets/images/buy-now.png'
        ),
        array(
          'id'    => 'product_buy_link',
          'type'  => 'text',
          'title' => 'Product buy link',
          'default' => 'http://erotictouch.shop/shop'
        ),
        array(
          'id'    => 'product_sel_price',
          'type'  => 'number',
          'title' => 'Product sel price',
          'default' => 39.95
        ),
        array(
          'id'    => 'product_regular_price',
          'type'  => 'number',
          'title' => 'Product buy link',
          'default' => 69.95
        ),
      ),
    ),


  )

);
/***
* Contact page options
*/
$options[] = array(
  'name'  => 'contacPage_options',
  'title' => 'Contact Page Options',
  'icon'  => 'fa fa-star',
  'fields'  => array(
    array(
      'id'  => 'contact_page_security_image',
      'title' => 'Contact page topBar security image',
      'type'  => 'upload',
      'default' => 'http://localhost/sajidvai-new/wp-content/themes/erotic-theme/assets/images/nortonmcafee.png',
      'desc'  => 'Contact page top-bar security image. At the right of logo image on the top.'
    ),
    array(
      'id'  => 'contact_page_guarantee_image',
      'title' => 'Contact page topBar Guarantee image',
      'type'  => 'upload',
      'default' => 'http://localhost/sajidvai-new/wp-content/themes/erotic-theme/assets/images/madeinusa.png',
      'desc'  => 'Contact page top-bar Guarantee image. At the right of logo image and security image on the top.'
    ),
    array(
      'id'  => 'contact_page_phoneUs_text',
      'title' => 'Contact page Phone us text',
      'type'  => 'text',
      'default' => 'To order SizeGenix Call',
      'desc'  => 'Contact page phone us area text. Before phone number.'
    ),
    array(
      'id'  => 'contact_page_phoneUs_number',
      'title' => 'Contact page Phone Number',
      'type'  => 'text',
      'default' => '1-800-801-0403',
      'desc'  => 'Contact page phone us area phone number.'
    ),
    array(
      'id'  => 'contact_page_top_text',
      'title' => 'Contact page Form top text',
      'type'  => 'wysiwyg',
      'default' => '<span class="align-self-end">Superbalife International</span>
                    <span class="align-top"><img src="http://localhost/sajidvai-new/wp-content/themes/erotic-theme/assets/images/ico-flag.png" alt="" class="align-top"></span>
                  </div>
                <div class="h5 font-italic">1171 S Robertson Blvd #525, Los Angeles, CA 90035</div>',
      'desc'  => 'Contact page Form top text. on the top of contact page form before title.'
    ),
    array(
      'id'  => 'contact_page_form_title',
      'title' => 'Contact page Form title',
      'type'  => 'text',
      'default' => 'Leave a message',
      'desc'  => 'Contact page form title on the top of contact form. It is a bit title.'
    ),
    array(
      'id'  => 'contact_page_sidebar_image',
      'title' => 'Contact page image on sidebar',
      'type'  => 'upload',
      'default' => 'http://www.erotictouch.shop/wp-content/themes/erotic-theme/assets/images/contact-show.png',
      'desc'  => 'Contact page Image on sidebar right.'
    ),

  )
);

/***
* Footer options 
*/
$options[]      = array(
  'name'  => 'footer_options',
  'title' => 'Footer Options',
  'icon'  => 'fa fa-star',
  'fields' => array(
    array(
      'id'        => 'footer_slogan',
      'title'     => 'Footer small text',
      'type'      => 'text',
      'default'   => esc_html('&quot;† These statements have not been evaluated by the Food and Drug Administration. These products are not intended to diagnose, treat, cure, or prevent any disease. Results not typical. Individual results may vary.'),
      'desc' => 'Footer small text at footer top.',
    ),
    array(
      'id'        => 'footer_logo',
      'title'     => 'Footer logo',
      'type'      => 'upload',
      'default'   => 'http://www.sizegenix.com/img/footerlogo-1.png',
    ),
    array(
      'id'        => 'footer_copyright',
      'title'     => 'Footer copyright text',
      'type'      => 'wysiwyg',
      'default'   => '© COPYRIGHT 2014-2015 | SIZEGENIX.COM | INCREDIBLE HEALTH DECISIONS, LLC',
    ),
    array(
      'id'        => 'footer_bottom_text',
      'title'     => 'Footer bottom text',
      'type'      => 'wysiwyg',
      'default'   => '<a onclick="newWindow(\'/terms.html\', 680, 0);" style="cursor: pointer;">Terms &amp; Conditions</a> | <a onclick="newWindow(\'/privacy.html\', 680, 0);" style="cursor: pointer;">Privacy Policy</a> | <a onclick="newWindow(\'/dmca.html\', 680, 0);" style="cursor: pointer;">DMCA</a>',
    ),
  )
);


CSFramework::instance( $settings, $options );
