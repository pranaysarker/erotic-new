<?php
	add_action( 'after_setup_theme','erotic_after_setup_theme' );
	function erotic_after_setup_theme(){
		add_theme_support( 'woocommerce' );
		add_theme_support( 'custom-logo' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'custom-header' );

		register_nav_menus( array(
			'main-menu' => 'Main menu',
			'footer-menu' => 'Footer Menu'
		) );
	}

	// requiring all files
	require_once( __DIR__.'/lib/erotic-nav-walker.php');
	require_once( __DIR__.'/lib/erotic-footer-nav-walker.php');
	// codestar framework
	require_once( __DIR__.'/lib/codestar/cs-framework.php');

	// woocommerce hook remove
	require_once(__DIR__.'/inc/woocommerce-func.php');

