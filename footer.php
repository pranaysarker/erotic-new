	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-10 p-0 m-0">
					<nav class="navbar navbar-expand-md navbar-light p-0 m-0">
						<div class="p-0 m-0 text-center">
							<?php wp_nav_menu( array(
								'theme_location'  => 'footer-menu',
								'menu'            => 'footer-menu',
								'container'       => '',
								'menu_class'      => 'navbar-nav text-center m-auto',
								'fallback_cb'     => 'wp_page_menu',
								'walker'          => new Erotic_footer_nav_walker,
							) ); ?>
							<!-- <ul class="navbar-nav text-center m-auto">
								<li class="nav-item">
									<a href="index.html" class="nav-link text-uppercase h3 m-0 p-0 active">Home</a>
								</li>
								<li class="nav-item">
									<a href="#" class="nav-link text-uppercase h3 m-0 p-0">How It Works?</a>
								</li>
								<li class="nav-item">
									<a href="#" class="nav-link text-uppercase h3 m-0 p-0">Faq</a>
								</li>
								<li class="nav-item">
									<a href="#" class="nav-link text-uppercase h3 m-0 p-0">Testimonials</a>
								</li>
								<li class="nav-item">
									<a href="#" class="nav-link text-uppercase h3 m-0 p-0">Contact Us</a>
								</li>
								<li class="nav-item">
									<a href="#" class="nav-link text-uppercase h3 m-0 p-0">Order Now</a>
								</li>
							</ul> -->
						</div>
					</nav>
				</div>
				<div class="col-md-2">
					<img src="<?php 
							if( !empty( cs_get_option('footer_logo') ) ){
								echo cs_get_option('footer_logo');
							} 
						?>" alt="">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<p class="footer-small-desc">
						<?php 
							if( !empty( cs_get_option('footer_slogan') ) ){
								echo cs_get_option('footer_slogan');
							} 
						?>	
					</p>
					<p style="text-align:center; margin-bottom: 5px;">
						<?php 
							if( !empty( cs_get_option('footer_copyright') ) ){
								echo cs_get_option('footer_copyright');
							} 
						?>
					</p>
					<p class="copy-link" style="text-align:center; margin-top: 5px;">
						<?php 
							if( !empty( cs_get_option('footer_bottom_text') ) ){
								echo cs_get_option('footer_bottom_text');
							} 
						?>	
					</p>
				</div>
			</div>
		</div>
	</footer>

  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-3.3.1.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.magnific-popup.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/popper.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/main.js"></script>
  <script>
    var containerEl = document.querySelector('.mixitup-play');
    var mixer = mixitup(containerEl);



</script>
<?php wp_footer(); ?>
</body>
</html>